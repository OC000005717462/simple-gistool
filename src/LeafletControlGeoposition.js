// import { createControlComponent } from "@react-leaflet/core";
import React from "react";
import { useMap } from "react-leaflet";
import L from "leaflet";

class LeafletControlGeoposition extends React.Component {
  constructor(props) {
    super(props);
    this.state = { render: true };
  }
  createControl(MyProps) {
    const LeafletControlGeoposition = L.Control.extend({
      onAdd: (map) => {
        const panelDiv = L.DomUtil.create("div", "LeafletControlGeoposition");
        panelDiv.innerHTML = "<span></span>";

        map.addEventListener("mousemove", (ev) => {
          if (this.state.render) {
            panelDiv.innerHTML = `<h2><span>Lat: ${ev.latlng.lat.toFixed(
              4
            )}</span>&nbsp;<span>Lng: ${ev.latlng.lng.toFixed(4)}</span></h2>`;
          }
        });
        return panelDiv;
      },
      onRemove: (map) => {
        console.log("LeafletControlGeoposition Remove");

        // Nothing to do
      }
    });

    return new LeafletControlGeoposition({ position: MyProps.position });
  }

  componentDidMount() {
    const { map } = this.props;

    const control = this.createControl(this.props);
    control.addTo(map);
  }

  render() {
    return null;
  }
}

function withMap(Component) {
  return function WrappedComponent(props) {
    const map = useMap();
    return <Component {...props} map={map} />;
  };
}

export default withMap(LeafletControlGeoposition);

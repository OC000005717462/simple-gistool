import MapView from "./MapView";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Route
        path={[
          "/",
          "/:lat/:lng",
          "/:lat/:lng/:zoom",
          "/:lat/:lng/:zoom/:option"
        ]}
        exact
        component={MapView}
      />
    </Router>
  );
}

export default App;

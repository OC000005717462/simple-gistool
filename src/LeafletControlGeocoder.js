// import { createControlComponent } from "@react-leaflet/core";
import React from "react";
import { useMap } from "react-leaflet";
import L from "leaflet";
import "leaflet-control-geocoder/dist/Control.Geocoder.css";
import "leaflet-control-geocoder/dist/Control.Geocoder.js";


// import icon from "./constants";


class LeafletControlGeocoder extends React.Component {
  constructor(props) {
    super(props);
    this.state = { render: true };
  }
  createControl(MyProps) {


    const LeafletControlGeocoder = L.Control.extend({
      onAdd: (map) => {
        var geocoder = L.Control.Geocoder.nominatim();
        /* eslint no-restricted-globals:0 */
        if (typeof URLSearchParams !== "undefined" && location.search) {
          // parse /?geocoder=nominatim from URL
          var params = new URLSearchParams(location.search);
          var geocoderString = params.get("geocoder");
          if (geocoderString && L.Control.Geocoder[geocoderString]) {
            geocoder = L.Control.Geocoder[geocoderString]();
          } else if (geocoderString) {
            console.warn("Unsupported geocoder", geocoderString);
          }
        }


        L.Control.geocoder({
          position: MyProps.position,
          query: "",
          placeholder: "Search here...",
          defaultMarkGeocode: false,
          geocoder
        })
          .on("markgeocode", function (e) {
            // var latlng = e.geocode.center;
            
            // L.marker(latlng, { icon })
            //   .addTo(map)
            //   .bindPopup(e.geocode.name)
            //   .openPopup();
            map.fitBounds(e.geocode.bbox,{animate: true, duration: 5.0});
            // map.panTo(latlng,{animate: true, duration: 1.0});
            })
          .addTo(map);
      // } );

        const panelDiv = L.DomUtil.create("div", "info");
        panelDiv.innerHTML = "<span></span>";
      return panelDiv;











        // const panelDiv = L.DomUtil.create("div", "info");
        // panelDiv.innerHTML = "<span></span>";

        // map.addEventListener("mousemove", (ev) => {
        //   if (this.state.render) {
        //     panelDiv.innerHTML = `<h2><span>Lat: ${ev.latlng.lat.toFixed(
        //       4
        //     )}</span>&nbsp;<span>Lng: ${ev.latlng.lng.toFixed(4)}</span></h2>`;
        //   }
        // });
        // return panelDiv;
      },
      onRemove: (map) => {
        console.log("LeafletControlGeocoder Remove");

        // Nothing to do
      }
    });

    return new LeafletControlGeocoder({ position: MyProps.position });
  }

  componentDidMount() {
    const { map } = this.props;

    const control = this.createControl(this.props);
    control.addTo(map);
  }

  render() {
    return null;
  }
}

function withMap(Component) {
  return function WrappedComponent(props) {
    const map = useMap();
    return <Component {...props} map={map} />;
  };
}

export default withMap(LeafletControlGeocoder);

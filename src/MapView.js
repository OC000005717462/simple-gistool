import React from "react";
// import ReactDOM from 'react-dom';

import {
  MapContainer,
  useMapEvents,
  TileLayer,
  WMSTileLayer,
  Marker,
  LayersControl
} from "react-leaflet";

// import L from "leaflet";

import "leaflet/dist/leaflet.css";
import "leaflet-contextmenu";
import "leaflet-contextmenu/dist/leaflet.contextmenu.css";

import LeafletControlGeocoder from "./LeafletControlGeocoder";
import LeafletControlGeoposition from "./LeafletControlGeoposition";
import Popup from "react-leaflet-editable-popup";

import {
  blueIcon,
  greenIcon,
  blackIcon,
  violetIcon,
  radioTowerIcon
} from "./Icons";

import "./styles.css";

const GLabels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

const IconsArray = [blueIcon, greenIcon, blackIcon, violetIcon, radioTowerIcon];

// Add geocoordinates to URL
function MyMoveEndEventHandler() {
  const map = useMapEvents({
    moveend: () => {
      console.log("moveend ");
      // var data = map.getBounds();
      var zoom = map.getZoom();
      var center = map.getCenter();

      window.history.pushState(
        {},
        "",
        "/" + center.lat + "/" + center.lng + "/" + zoom
      );
    },
    movestart: () => {
      console.log("movestart");
    },
    viewreset: () => {
      console.log("viewreset");
    },
    layeradd: () => {
      console.log("layeradd");
    }
  });
  return null;
}

// MapView Class
class MapView extends React.Component {
  constructor(props) {
    super(props);

    if (this.props.match.params.lat && this.props.match.params.lng) {
      let izoom = 12;
      let sOption = "";
      if (this.props.match.params.zoom) {
        izoom = this.props.match.params.zoom;
        sOption = this.props.match.params.option;
        if (sOption) {
          sOption = sOption.toString().toUpperCase();
        }
      }
      this.state = {
        currentLocation: {
          lat: this.props.match.params.lat,
          lng: this.props.match.params.lng
        },
        zoom: izoom,
        option: sOption
      };
    } else {
      this.state = {
        currentLocation: { lat: 49.87628, lng: 8.12774 },
        zoom: 5
      };
    }
  }

  //componentDidMount
  componentDidMount() {
    console.log("componentDidMount");

    if (this.state.option === "M") {
      let aMarker = [
        this.props.match.params.lat,
        this.props.match.params.lng,
        0
      ];
      console.log("add first Marker " + aMarker);

      this.setState({
        MarkerID: 0,
        MarkerArray: [aMarker],
        LabelArray: [],
        distanceMarker1: null,
        Ggeocoder: null,
        GeditGeoBoxechangedEventFire: true,
        changetm: null
      });
    } else {
      this.setState({
        MarkerID: 0,
        MarkerArray: [],
        LabelArray: [],
        distanceMarker1: null,
        Ggeocoder: null,
        GeditGeoBoxechangedEventFire: true,
        changetm: null
      });
    }
  }

  //-------------------------------------------------------
  // add Marker
  addMarker = (e) => {
    console.log("addMarker ");
    // this.setState({ MarkerArray: [...this.state.MarkerArray, e.latlng] });

    // let _MarkerID = this.state.MarkerID;

    let _MarkerID = this.state.MarkerArray.length;

    console.log("addMarker " & _MarkerID);
    this.setState({
      MarkerArray: [...this.state.MarkerArray, [e.latlng.lat, e.latlng.lng, 0]]
      // MarkerID: _MarkerID + 1
    });
  };

  //-------------------------------------------------------
  // changeIcon
  changeIcon = (e) => {
    console.log("changeIcon: ");

    // GetIdx
    let id = e.relatedTarget.options.marker_index; //get marker index

    if (id !== -1) {
      let newIconId = 0;
      this.setState((state) => {
        const MarkerArray = this.state.MarkerArray.map((item, j) => {
          if (j === id) {
            newIconId = item[2] + 1;
            if (newIconId >= IconsArray.length) {
              newIconId = 0;
            }
            return [item[0], item[1], newIconId];
          } else {
            return item;
          }
        });
        // console.log("Ende 1");
        // console.log(MarkerArray);
        return {
          MarkerArray
        };
      });
    }
  };

  //-------------------------------------------------------
  // Delete Marker
  delMarker = (e) => {
    console.log("delMarker: ");
    console.log(e);

    // GetIdx
    let id = e.relatedTarget.options.marker_index; //get marker index
    console.log(id);

    if (id !== -1) {
      this.setState((state) => {
        const MarkerArray = state.MarkerArray.filter((item, j) => id !== j);

        return {
          MarkerArray
        };
      });

      //      const { map } = this.state;
      //    map.removeLayer(e.relatedTarget);
    }
  };

  //-------------------------------------------------------
  // setCircle
  setCircle = (e) => {
    console.log("setCircle: ");
    console.log(e);
    // GetIdx
    let id = e.relatedTarget.options.marker_index; //get marker index
    console.log(id);
  };

  //-------------------------------------------------------
  // center map here
  centerMap = (e) => {
    console.log("centerMap ");
    const MarkerArray = this.state.MarkerArray;
    console.log(MarkerArray);

    const { map } = this.state;
    map.panTo(e.latlng);
  };

  //-------------------------------------------------------
  // Zoom in
  zoomIn = () => {
    console.log("ZoomIn");

    const { map } = this.state;
    map.zoomIn();
  };

  //-------------------------------------------------------
  // Zoom out
  zoomOut = () => {
    console.log("ZoomOut");

    const { map } = this.state;

    map.zoomOut();
  };
  //-------------------------------------------------------
  updateMarker = (event) => {
    console.log("updateMarker ");

    const latLng = event.target.getLatLng(); //get marker LatLng
    const markerIndex = event.target.options.marker_index; //get marker index

    const Markervalue = this.state.MarkerArray[markerIndex];

    Markervalue[0] = latLng.lat;
    Markervalue[1] = latLng.lng;
  };

  //-------------------------------------------------------
  // render
  //-------------------------------------------------------
  render() {
    const currentLocation = this.state.currentLocation;
    const zoom = this.state.zoom;

    const MarkerArray = this.state.MarkerArray;
    // , setMarkerArray] = useState([]);
    console.log(MarkerArray);

    return (
      <MapContainer
        className="markercluster-map"
        center={currentLocation}
        zoom={zoom}
        maxZoom={18}
        closePopupOnClick={false}
        scrollWheelZoom={true}
        contextmenu={true}
        contextmenuItems={[
          {
            text: "Center map here",
            callback: this.centerMap
          },
          {
            text: "Add marker",
            callback: this.addMarker
          },
          {
            text: "Zoom in",
            callback: this.zoomIn
          },
          { text: "Zoom out", callback: this.zoomOut }
        ]}
        whenCreated={(map) => this.setState({ map })}
      >
        <LayersControl position="bottomright">
          {/* 
        <LayersControl.BaseLayer checked name="WMS">
          <WMSTileLayer url="https://apps.ecmwf.int/wms/?" 
        token= "public"
        version= '1.3'
        format= 'image/png'
        transparent={true}
        tiles={true}
        uppercase={true}
        layers= 'background,named_cyclones,named_cyclones_tracks,foreground'
          />
           
        </LayersControl.BaseLayer> */}

          <LayersControl.BaseLayer checked name="BKG">
            <TileLayer
              url="http://sgx.geodatenzentrum.de/wmts_topplus_web_open/tile/1.0.0/web/default/WEBMERCATOR/{z}/{y}/{x}.png"
              attribution='&copy; <a href=\"http://www.bkg.bund.de\" target=\"_new\">Bundesamt f&uuml;r Kartographie und Geod&auml;sie</a> 2017, <a href="https://sg.geodatenzentrum.de/web_public/Datenquellen_TopPlus_Open.pdf" target=\"_new\">Datenquellen</a>'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS Webatlas">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_webatlasde"
              layers="webatlasde"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS p5">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_topplus_open"
              layers="p5"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS p25">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_topplus_open"
              layers="p25"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS p50">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_topplus_open"
              layers="p50"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS p100">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_topplus_open"
              layers="p100"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS p250">
            <WMSTileLayer
              url="https://sgx.geodatenzentrum.de/wms_topplus_open"
              layers="p250"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG WMS Sentinel2">
            <WMSTileLayer
              url="https://sg.geodatenzentrum.de/wms_sentinel2_de"
              layers="rgb"
              format="image/png"
              transparent={true}
              tiles={true}
              onTileerror={console.warn}
              onLoading={console.log}
              onLoad={console.log}
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="ESRI Sattelite ">
            <TileLayer
              url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
              attribution="Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community"
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG web_grau">
            <TileLayer
              url="http://sgx.geodatenzentrum.de/wmts_topplus_web_open/tile/1.0.0/web_grau/default/WEBMERCATOR/{z}/{y}/{x}.png"
              attribution='&copy; <a href=\"http://www.bkg.bund.de\" target=\"_new\">Bundesamt f&uuml;r Kartographie und Geod&auml;sie</a> 2017, <a href="https://sg.geodatenzentrum.de/web_public/Datenquellen_TopPlus_Open.pdf" target=\"_new\">Datenquellen</a>'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG web_scale">
            <TileLayer
              url="http://sgx.geodatenzentrum.de/wmts_topplus_web_open/tile/1.0.0/web_scale/default/WEBMERCATOR/{z}/{y}/{x}.png"
              attribution='&copy; <a href=\"http://www.bkg.bund.de\" target=\"_new\">Bundesamt f&uuml;r Kartographie und Geod&auml;sie</a> 2017, <a href="https://sg.geodatenzentrum.de/web_public/Datenquellen_TopPlus_Open.pdf" target=\"_new\">Datenquellen</a>'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="BKG web_scale_grau">
            <TileLayer
              url="http://sgx.geodatenzentrum.de/wmts_topplus_web_open/tile/1.0.0/web_scale_grau/default/WEBMERCATOR/{z}/{y}/{x}.png"
              attribution='&copy; <a href=\"http://www.bkg.bund.de\" target=\"_new\">Bundesamt f&uuml;r Kartographie und Geod&auml;sie</a> 2017, <a href="https://sg.geodatenzentrum.de/web_public/Datenquellen_TopPlus_Open.pdf" target=\"_new\">Datenquellen</a>'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="OpenStreetMap Deutschland">
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"
            />
          </LayersControl.BaseLayer>

          {/* <LayersControl.BaseLayer checked name="BKG VG">
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://sgx.geodatenzentrum.de/wfs_vg2500?service=wfs&version=1.1.0&request=GetFeature&TYPENAME=Kreis&BBOX={s},{z},{x},{y}"
            />
          </LayersControl.BaseLayer> */}

          <LayersControl.BaseLayer name="OpenStreetMap.Mapnik">
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </LayersControl.BaseLayer>
          <LayersControl.BaseLayer name="OpenStreetMap.BlackAndWhite">
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
            />
          </LayersControl.BaseLayer>
        </LayersControl>
        <MyMoveEndEventHandler />

        <LeafletControlGeocoder position={"topleft"} />
        <LeafletControlGeoposition position={"topright"} />

        {/* ---------------   Marker --------------------------- 
        Solve problem on 
        https://stackoverflow.com/questions/53342547/react-leaflet-updating-coords-of-multiple-markers-in-state
        */}
        {this.state.MarkerArray &&
          this.state.MarkerArray.map((marker, index) => (
            <Marker
              key={index}
              marker_index={index}
              icon={IconsArray[marker[2]]}
              position={[marker[0], marker[1]]}
              draggable={true}
              eventHandlers={{
                moveend: this.updateMarker
              }}
              contextmenu={true}
              contextmenuItems={[
                {
                  text: "Delete marker",
                  callback: this.delMarker
                  // onClick={() => this.delMarker(index)}
                },
                {
                  text: "Change icon",
                  callback: this.changeIcon
                },
                {
                  text: "circle",
                  callback: this.setCircle
                }
              ]}
            >
              <Popup
                editable
                open
                autoClose={false}
                closeButton={false}
                maxlenth={5}
                key={`popup-${index}`}
              >
                {GLabels[index]}
              </Popup>
            </Marker>
          ))}
      </MapContainer>
    );
  }
}

export default MapView;
